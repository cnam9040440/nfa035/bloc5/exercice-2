package fr.cnam.foad.nfa035.badges.wallet.model;

import java.util.Set;

public class DigitalWallet {

    private Set<DigitalBadge> allBadges;
    private Set<Long> deadLinesPositions;

    private Set<DigitalBadge> deletingBadges;

    private Set<DigitalBadge> badges;

    private DigitalBadge suppression;

    public DigitalWallet(Set<DigitalBadge> allBadges, Set<Long> deadLinesPositions, Set<DigitalBadge> deletingBadges) {
        this.allBadges = allBadges;
        this.deadLinesPositions = deadLinesPositions;
        this.deletingBadges = deletingBadges;
    }

    /**
     * méthode ajoutant un badge à la liste des badges potentiellement à supprimer
      * @param badge
     */
    public void addDeletingBadges(DigitalBadge badge){
        Set<DigitalBadge> deletingBadges = this.getDeletingBadges();
        if(!deletingBadges.contains(badge)){
            deletingBadges.add(badge);
        }
    }

    /**
     *
     */
    public void commitBadgeDeletion(){

    }

    /**
     * méthodes récupérant la liste des badges
     * @return
     */
    public Set<DigitalBadge> getAllBadges() {
        return allBadges;
    }

    /**
     * méthode renseignant la liste des badges dans le wallet
     * @param allBadges
     */
    public void setAllBadges(Set<DigitalBadge> allBadges) {
        this.allBadges = allBadges;
    }

    /**
     * méthode retournant la position des lignes mortes une fois la suppression effective
     * @return
     */
    public Set<Long> getDeadLinesPositions() {
        return deadLinesPositions;
    }

    /**
     * méthode renseignant la position des lignes mortes une fois la suppression effective
     * @param deadLinesPositions
     */
    public void setDeadLinesPositions(Set<Long> deadLinesPositions) {
        this.deadLinesPositions = deadLinesPositions;
    }

    /**
     * méthode permettant de récupérer les instances de badges potentiellement à supprimer
     * @return
     */
    public Set<DigitalBadge> getDeletingBadges() {
        return deletingBadges;
    }

}
